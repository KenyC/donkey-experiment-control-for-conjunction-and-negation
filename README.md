---
title: Verification of the interpretation of conjunction under the scope of an existential
authors: Keny Chatain, Benjamin Spector
---

# Introduction

In a [previous experiment](https://osf.io/afqm4), hereafter *the pronoun experiment*, we asked participants to rate how true sentences like [@anaphoraconjunction a] and [@anaphoraconjunction b] felt to them in various scenarios.

:::{.ex id=anaphoraconjunction}
a. There is a triangle and it is blue.
b. There is a triangle and it is not blue.
:::

<!--
:::{.ex id=posscenarios}
a. POS-CONJ1FALSE: there is no triangle
a. POS-CONJ2FALSE: there is only one triangle ; that triangle is not blue
a. POS-EXISTS: there is one blue triangle, there is one non-blue triangle, other shapes are not triangles
a. POS-ALL: there are two blue triangles, other shapes are not triangle
:::

:::{.ex id=negscenarios}
a. NEG-CONJ1FALSE: there is no triangle
a. NEG-CONJ2FALSE: there is only one triangle ; that triangle is blue
a. NEG-EXISTS: there is one blue triangle, there is one non-blue triangle, other shapes are not triangles
a. NEG-ALL: there are two non-blue triangles, other shapes are not triangle
:::
-->

We found significant differences between the interpretation of a sentences like  [@anaphoraconjunction a] and [@anaphoraconjunction b]. While [@anaphoraconjunction a] was interpreted in conformance with the existential interpretation in [@existinterpretation], [@anaphoraconjunction b] showed considerably more variation and participants ascribed significantly lower scores to the sentence in pictures that made the existential but not the universal interpretation in [@universalinterpretation] (picture type NEG-EXISTS, cf below)

:::{.ex id=existinterpretation}
Existential interpretation

a. [@anaphoraconjunction a]: *there is a blue triangle*
b. [@anaphoraconjunction b]: *there is a non-blue triangle*
:::

:::{.ex id=universalinterpretation}
Universal interpretation

a. [@anaphoraconjunction a]: *there is a triangle and all triangles are blue*
b. [@anaphoraconjunction b]: *there is a triangle and no triangles are blue*
:::


The current experiment (hereafter *the no-pronoun experiment*) aims to supplement the result of the pronoun experiment with a control without pronouns. The no-pronoun experiment follows the same protocol as the pronoun experiment replacing [@anaphoraconjunction a] and [@anaphoraconjunction b] with [@noanaphoraconjunction a] and [@noanaphoraconjunction b] respectively. We reuse the exact same picture used in the previous experiments.

:::{ .ex id=noanaphoraconjunction}
a. There is a circle that is blue.
b. There is a circle that is not blue.
:::

Our goal is to check that the interpretation of sentences without pronouns is, as expected in the theory, the existential interpretation, even with negation present. To check this, we will compare the results obtained in this no-pronoun experiment to those of the pronoun experiment (*between-subject comparison*).

# Design

Participants complete a series of 30 trials. Each trial consists of a sentence and a picture. For each trial, participants are asked to rate "*how true the sentence feels to \[them]*" on a scale ranging from 1 (*completely false*) to 7 (*completely true*). Each trial belongs to a condition which is a pair of a sentence type with a picture type. Each condition has 3 trials. We start by describing the picture type used and the sentence type. Below is an image showing a typical trial:

<!-- TODO -->

## Sentence types

 1. **POS:** There is a SHAPE that is COLOR.
 2. **NEG:** There is a SHAPE that is not COLOR.
 3. **CONTROL:** There is a SHAPE1 and the SHAPE2 is COLOR.

Where:

 - SHAPE, SHAPE1, SHAPE2 are either *circle*, *triangle* or *square*
 - COLOR is either *red*, *green* or *blue*

## Picture types

Each picture contains 4 colored shapes displayed a square configuration (cf picture above). We describe the picture types for the case SHAPE = triangle, COLOR = blue
 
 - **POS-CONJ1FALSE:** there is no triangle
 - **POS-CONJ2FALSE:** there is only one triangle ; that triangle is not blue
 - **POS-EXISTS:** there is one blue triangle, there is one non-blue triangle, other shapes are not triangles
 - **POS-ALL:** there are two blue triangles, other shapes are not triangle


 - **NEG-CONJ1FALSE:** there is no triangle
 - **NEG-CONJ2FALSE:** there is only one triangle ; that triangle is blue
 - **NEG-EXISTS:** there is one blue triangle, there is one non-blue triangle, other shapes are not triangles
 - **NEG-ALL:** there are two non-blue triangles, other shapes are not triangle

For the CONTROL sentence type, we describe the picture for SHAPE1 = triangle, SHAPE2 = circle, COLOR = blue

 - **CONTROL-BOTHCONJFALSE:** no triangle, exactly one circle, the circle is not blue
 - **CONTROL-TRUE:** exactly one triangle, exactly one circle, the circle is blue


## Conditions

Conditions are named after picture types and they are associated with the corresponding sentence type. For instance, a trial of the POS-CONJ1FALSE condition consists of a picture of the POS-CONJ1FALSE picture type associated with a sentence of the POS type.


# Hypothesis

 - **Hypothesis:** *non-existential interpretations are more easily accessed in sentences with pronouns than they are in sentences without* 
 - *Operationalized as:* There is an interaction between POS-EXISTS and NEG-EXISTS condition in the pronoun experiment and the POS-EXISTS and NEG-EXISTS of the current no-pronoun experiement (formulated in terms of cumulative link model, cf [[#Analysis plan]])


# Sampling plan

## Data collection procedures

We will recruit participants through the Prolific platform, selecting only native speakers of English who are not colorblind. Each participant will be paid £2.25


## Sample size

We plan to recruit 120 participants, the same number of participants that we got from the previous experiment


# Analysis plan

## Statistical models

We want to know whether the difference between the conditions POS-EXIST and NEG-EXIST that we observed in the pronoun Experiment (a difference that we did not directly test) is also observed to a similar extent in the experiment we are describing here (the no-pronoun experiment). To this end, we will subset the data from the pronoun experiment restricted to these two conditions, and do the same with the data from the current no-pronoun experiment. We therefore will have data for 4 conditions, corresponding to 2 2-level factors: set (pos vs. neg), pronoun (pronoun vs. no pronoun). The polarity factor is within participants, while the pronoun factor is between participants. We want to test whether there is a statistically significant interaction between these two factors.  
  
We will fit a cumulative link model with logit link that will include slopes for the two conditions as well as an interaction term as fixed effect, and a random intercept per participant as a random effect, and compare this model with a purely additive model without the interaction term (i.e. the model that results from removing the product term). We will compare the two models using the *anova* function. We will consider the interaction to be statistically significant if the p-value returned by this model comparison (likelihood ration test) is below 0.05, correcting for multiple comparisons, taking into account the analyses we already ran for the Pronoun Experiment. In the pronoun Experiment, we reported 5 p-values. We will therefore have 6 p-values in total, and we will apply the Holm-Bonferroni method to correct these 6 p-values to correct for multiple comparisons.  
  
Cf attached R script for details.

<!-- TODO -->

## Data exclusion

 - We will exclude participants who report (in the final questionnaire) that they are colorblind, or that they are not native speakers of English.
 - We will exclude any participant who completed the experiment more than once
 - We will exclude participants who, at least twice (out of six trials), give normatively incorrect answers to CONTROL-BOTHCONJFALSE and CONTROL-TRUE conditions, where a normatively incorrect answer is:
    * for CONTROL-TRUE condition: any score of 4 or less
    * for CONTROL-BOTHCONJFALSE condition: any score of 3 or more